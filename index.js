window = {}

const express = require('express')
const bodyParser = require('body-parser')
const cors = require("cors")
const app = express()
const swaggerUi = require('swagger-ui-express')
const mongoose = require('mongoose')

const userRouter = require('./routes/user')
const allUsersRouter = require('./routes/users')
const loginRouter = require('./routes/login')
const contactRouter = require('./routes/contact')
const leaveRouter = require('./routes/leaves')

const swaggerDocument = require('./swagger.json')

mongoose.connect('mongodb://localhost:27017/craftersHUB', { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false })
  .then(() => console.log('I am connected'))
  .catch(error => console.log(error));

app.use(bodyParser.json())
app.use(cors())

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.get('/', (_, res) => {
  res.json({ message: "hello there" })
})

// This is for test api call

app.get('/dummy', (_, res) => {
  res.json({ message: "Test is passed" })
})

// routers

app.use('/user', userRouter)
app.use('/users', allUsersRouter)
app.use('/login', loginRouter)
app.use('/contact', contactRouter)
app.use('/leaves', leaveRouter)

app.listen(3000, () => console.log('server started'))