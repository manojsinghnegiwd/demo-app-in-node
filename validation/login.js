module.exports = {
    email: {
      required: {
        errorMsg: 'Email is required'
      },
      email: {
        errorMsg: 'Email is invalid'
      }
    },
    password: {
      required: {
        errorMsg: 'Password is required'
      }
    }
}