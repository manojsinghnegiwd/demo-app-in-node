const is = require("is_js")
const bcrypt = require("bcrypt")
const { Router } = require("express")
const { validate } = require("micro-validator").default

const userValidations = require("../validation/user")
const clockInValidation = require("../validation/clockPayload")
const { userModel } = require("../models/user")
const { attendanceModel } = require("../models/attendance")

const userRouter = Router()

const validClockingTypes = ["in", "lunchIn", "lunchOut", "out"]

const saltRounds = 10

const generatePassword = (rawPassword = "") =>
  new Promise((resolve, reject) => {
    bcrypt.hash(rawPassword, saltRounds, function(err, hash) {
      if (err) {
        reject(err)
      }
      resolve(hash)
    })
  })

userRouter.post("/", async (req, res) => {
  const validationErrors = validate(userValidations, req.body)

  if (!is.empty(validationErrors)) {
    return res.status(400).json({ errors: validationErrors })
  }

  // Find if user exist

  try {
    const record = await userModel.find({ email: req.body.email })

    // If exist
    if (record.length) {
      res.status(400).json({
        errors: {
          duplicate: ["User with this email id already exist"],
        },
      })

      throw new Error("User with this email id already exist")
    }

    const hashedPassword = await generatePassword(req.body.password)

    const User = new userModel({ ...req.body, password: hashedPassword })

    User.save()
      .then(() => {
        res.status(200).json({ message: "User created successfully" })
      })
      .catch(err => {
        res
          .status(400)
          .json({ message: "Something went wrong. Unable to create user" })
      })
  } catch (error) {
    console.log(error)
  }
})


userRouter.get("/clock", async (_, res) => {
  try {
    const attendance = await attendanceModel.find({})
      return res.status(200).json({ attendance })
  } catch (error) {
      return res.status(502).json({ errors: [ "Some error occurred while fetching attendence" ] })
  }

})

userRouter.use("/:user_id", async (req, res, next) => {
  const { user_id = "" } = req.params

  if (!user_id.trim()) {
    return res.status(400).json({ errors: ["You have to provide a user_id"] })
  }

  try {
    const record = await userModel.findById(user_id)

    if (!record) {
      return res
        .status(400)
        .json({ errors: ["Please provide a valid user_id"] })
    } else {
      next()
    }
  } catch (error) {
    if (error.name == "CastError") {
      return res
        .status(400)
        .json({ errors: ["Please provide a valid user_id"] })
    }
  }
})

userRouter.put('/:user_id/edit', async (req, res) => {
  const { user_id = '' } = req.params
  let payload = req.body
  if(req.body.hasOwnProperty('password')){
    payload.password =  await generatePassword(req.body.password)
  }
  
  try {
    userModel.findByIdAndUpdate(user_id,  payload, {new: true})
      .then(() => res.status(200).json({ message: 'User updated successfully' }))
      .catch(error => console.log(error))

  } catch (error) {
    if (error.name == "CastError") {
      return res.status(400).json({ errors: ['Please provide a valid email'] })
    }
  }
})

userRouter.delete('/:user_id/delete', async (req, res) => {
  const { user_id = '' } = req.params
  try {
    userModel
      .findByIdAndRemove(user_id)
      .then(() =>
        res.status(200).json({ message: "User deleted successfully" })
      )
      .catch(error => console.log(error))
  } catch (error) {
    if (error.name == "CastError") {
      return res
        .status(400)
        .json({ errors: ["Please provide a valid user_id"] })
    }
  }
})

userRouter.post("/:user_id/clock", async (req, res) => {
  const { user_id = "" } = req.params
  const validationErrors = validate(clockInValidation, req.body)

  if (!is.empty(validationErrors)) {
    return res.status(400).json({ errors: validationErrors })
  }

  if (!validClockingTypes.includes(req.body.state)) {
    return res.status(400).json({
      errors: {
        state: ["You have to provide a valid state"],
      },
    })
  }

  try {
    const NewAttendanceRecord = new attendanceModel({
      ...req.body,
      user: user_id,
    })

    NewAttendanceRecord.save()
      .then(() => {
        res.status(200).json({ message: "Attendance recorded successfully" })
      })
      .catch(err => {
        res
          .status(400)
          .json({
            message: "Something went wrong. Unable to record attendance",
          })
      })
  } catch (error) {
    if (error.name == "CastError") {
      return res
        .status(400)
        .json({ errors: ["Please provide a valid user_id"] })
    }
  }
})

userRouter.get("/:user_id/clock", async (req, res) => {
  const { user_id = "" } = req.params
  try {
    const attendance = await attendanceModel
      .find({ user: user_id })
      .populate("user")
    return res.status(200).json({
      attendance,
    })
  } catch (error) {
    return res.status(502).json({ errors: ["Some error occurred"] })
  }
})

userRouter.get("/:user_id", async (req, res) => {
  const { user_id = "" } = req.params
  try {
    const user = await userModel.findOne({ _id: user_id })
    return res.status(200).json({
      user,
    })
  } catch (error) {
    return res.status(502).json({ errors: ["Some error occurred"] })
  }
})

module.exports = userRouter
