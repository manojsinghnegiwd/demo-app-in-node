const { Router } = require('express')

const allUsersRouter = Router()

const { userModel } = require('../models/user')

allUsersRouter.get('/', async (_, res) => {
  try {
    const users = await userModel.find({})

    return res.status(200).json({
      users
    })
  } catch (error) {
    return res.status(502).json({ errors: ['Some error occurred'] })
  }
})

module.exports = allUsersRouter
