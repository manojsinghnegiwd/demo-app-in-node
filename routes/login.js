const is = require("is_js")
const bcrypt = require("bcrypt")
const { Router } = require("express")
const { validate } = require("micro-validator").default
const loginValidations = require("../validation/login")
const { userModel } = require("../models/user")

const loginRouter = Router()

async function comparePassword(password, hashedPassword) {
  const match = await bcrypt.compare(password, hashedPassword)
  if (match) {
    return match
  } else {
    return false
  }
}

loginRouter.post("/", async (req, res) => {
  const validationErrors = validate(loginValidations, req.body)
  const { email, password } = req.body
  if (!is.empty(validationErrors)) {
    return res.status(400).json({ errors: validationErrors })
  }
  // Find if user exist
  try {
    const user = await userModel.findOne({ email: email })
    const hashedPassword = user.password
    const isPasswordMatched = await comparePassword(password, hashedPassword)
    // If exist
    if (user && isPasswordMatched) {
      res.status(200).json(user)
    } else {
      res.status(400).json({ error: "Invalid Credentials" })
    }
  } catch (err) {
    res.status(400).json({ error: "Invalid Credentials" })
  }
})

module.exports = loginRouter
