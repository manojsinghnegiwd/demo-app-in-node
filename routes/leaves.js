const is = require("is_js")
const { Router, response } = require("express")
const { validate } = require("micro-validator").default
const { leaveModal } = require('../models/leaves')
const leaveValidations = require("../validation/leaves") 
const leaveRouter = Router()



leaveRouter.get('/', async (_, res) => {
    try {
      const leaves = await leaveModal.find({})
  
      return res.status(200).json({
        leaves
      })
    } catch (error) {
      return res.status(502).json({ errors: ['Some error while fetching leaves'] })
    }
})

leaveRouter.post('/', async (req, res) => {
    const validationErrors = validate(leaveValidations, req.body)

    if(!is.empty(validationErrors)) {
        return res.status(400).json({ errors: validationErrors })
    }

    //Create a leave record
    const data =  new leaveModal({...req.body})
    data.save()
        .then(response => {
           return res.status(200).json({ response })
        })
        .catch(error => console.log(error))
})

leaveRouter.put('/:id', (req, res) => {
    const validationErrors = validate(leaveValidations, req.body)
    if(!is.empty(validationErrors)) {
        return res.status(400).json({ errors: validationErrors })
    }

    const { id = "",  } = req.params

    try {
        leaveModal.findByIdAndUpdate(id,  req.body, {new: true})
            .then(() => res.status(200).json({ message: 'Leave updated successfully', data: {...req.body}  }))
            .catch(error => console.log(error))
    }
    catch(error){
        console.log( error)
    }
})


leaveRouter.get('/:userId', async (req, res) => {
    const { userId = '' } = req.params
    try {
        const leaves = await leaveModal.find({ userId : userId })
        return res.status(200).json(leaves)
    }
    catch(error){
        return res.status(502)
            .json({ errors: ['Got some error while fetching data.']})
    }
})

leaveRouter.delete('/:leaveId',(req, res) => {

    const { leaveId = '' } = req.params
    try {
        leaveModal.findByIdAndRemove(leaveId)
        .then(() =>
            res.status(200).json({ message: "Leave record deleted successfully" })
        )
        .catch(error => console.log(error))
    } catch (error) {
        if (error.name == "CastError") {
            return res.status(400).json({ errors: ["Please provide a valid id"] })
        }
    }

})

module.exports = leaveRouter