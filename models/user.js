const { Schema, model } = require('mongoose')

const requiredStringDefObj = {
  required: true,
  type: String
}

const requiredNumberObj = {
  required : true, 
  type: Number
}

const userSchema = new Schema({
  firstName: requiredStringDefObj,
  lastName: requiredStringDefObj,
  email: requiredStringDefObj,
  password: requiredStringDefObj,
  employeeNo: requiredStringDefObj,
  role: requiredNumberObj,
  credits: { default: 0, type: Number },
  profilePicture: String
}, {
  timestamps: {
    createdAt: 'created_at'
  }
})

const userModel = new model('user', userSchema)

module.exports = {
  userModel,
  userSchema
}
